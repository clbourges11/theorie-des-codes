
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <bitset>
#include <cstdlib>
#include <ctime>
#include <algorithm>

const int N=2;
const int K=1;
const int R=4;

#define DEBUG

using namespace std;

////////////////////////////////////////////////////////////
//      template<int bits> bitset<bits> randBitset()      //
//                                                        //
//               Generate random bitset                   //
////////////////////////////////////////////////////////////

template<int bits> bitset<bits> randBitset()
{
  bitset<bits> r(rand());
  for(int i = 0; i < bits/16 - 1; i++)
  {
    r <<= 16;
    r |= bitset<bits>(rand());
  }
  return r;
}

////////////////////////////////////////////////////////////
// vector< bitset<N> > GSM_code(vector< bitset<K> > mess) //
//                                                        //
//     Convolutional coding of a message (GSM norm)       //
////////////////////////////////////////////////////////////

vector< bitset<N> > GSM_code(vector< bitset<K> > mess)
{
 int i=0, g0, g1;
 vector< bitset<N> > mess_out;

 bitset<N> cod_out;
 bitset<R+1> G0(25);
 bitset<R+1> G1(27);
 bitset<R+1> reg;
 reg.reset();

 #ifdef DEBUG
  cout << "-------------------- Debug Informations (Coding) --------------------" << endl << endl;
  cout << "Initial register ( u(i-4)  u(i-3)  u(i-2)  u(i-1)  u(i)  ): " << reg << endl;
  cout << "Polynom G0       ( g0(i-4) g0(i-3) g0(i-2) g0(i-1) g0(i) ): " << G0 << endl;
  cout << "Polynom G1       ( g1(i-4) g1(i-3) g1(i-2) g1(i-1) g1(i) ): " << G1 << endl << endl;
 #endif

 for (vector<bitset<K> >::iterator it = mess.begin(); it != mess.end(); ++it)
 {
  reg = reg<<1;
  reg.set(0,(*it).count());

  g0 = (reg&G0).count()%2;
  g1 = (reg&G1).count()%2;

  cod_out.reset();
  cod_out.set(0,g0);
  cod_out.set(1,g1);

  mess_out.push_back(cod_out);

  #ifdef DEBUG
   cout << "Block number: " << ++i << " - In frame: "<< *it << endl;
   cout << "\t Current status of registers: "<< reg << endl;
   cout << "\t Out : " << cod_out << endl;
  #endif
 }
 #ifdef DEBUG
  cout << "------------------------------------------------------------------" << endl << endl;
 #endif

 return mess_out;
}

/////////////////////////////////////////////////////////////////////////
// vector< bitset<N> >  GSM_transmission(vector< bitset<N> > mess_cod) //
//                                                                     //
//         Simulation of a transmission channel => adding errors       //
/////////////////////////////////////////////////////////////////////////

vector< bitset<N> >  GSM_transmission(vector< bitset<N> > mess_cod)
{
 vector< bitset<N> > mess_tra = mess_cod;
   for(int i=0;i<2;i++)
   {
      int index = rand() % mess_cod.size();
      mess_tra[index] = randBitset<N>();
   }
 return mess_tra;
}


//classe Chemin represente un chemin du diagramme
class Chemin{
    public:
        bitset<R> registre;
        vector< bitset<K> > mess_dec;
        int dist_cumul;
};

// La fonction comp permet de comparer deux chemins en interrogeant leurs distances_cumulées
bool comp (Chemin i,Chemin j) { return (i.dist_cumul<j.dist_cumul); }

// La fonction codage permet de calculer les bits de sortie à partir d'un état à 5 bits
bitset<N> codage(const bitset<R+1>& registre)
{
  bitset<N> cod_out;
	int g0, g1;
  bitset<R+1> G0(25);
  bitset<R+1> G1(27);
	g0 = (registre & G0).count() % 2;
	g1 = (registre & G1).count() % 2;

	cod_out.set(0, g0);
	cod_out.set(1, g1);
  return cod_out;
}


//////////////////////////////////////////////////////////////////
//				DISTANCE DE HAMMING		//
//////////////////////////////////////////////////////////////////

int distance_hamming(bitset<N> n1, bitset<N> n2)
{
	int compteur=0;
	if (n1[0] != n2[0])
	{
		compteur++;
	}
	if (n1[1] != n2[1])
	{
		compteur++;
	}
	return compteur;
}

//////////////////////////////////////////////////////////////////
// vector< bitset<K> > GSM_decode(vector< bitset<N> > mess_tra) //
//                                                              //
//     Convolutional decoding of a message (GSM norm)           //
//////////////////////////////////////////////////////////////////

//Decodage grâce à l'algorithme de Viterbi
vector< bitset<K> > GSM_decode(vector< bitset<N> > mess_tra)
{

    vector< bitset<K> > mess_dec;
    vector<Chemin> chemins;
    vector<Chemin> nouveaux_chemins;
	
    // On initialise le tout premier chemin du graphe
    Chemin chemin_init ;
    chemin_init.registre=0000;
    chemin_init.dist_cumul=0;
    chemins.push_back(chemin_init);

    vector< bitset<N> >::iterator it;
    for(it=mess_tra.begin();it!=mess_tra.end();it++)  // Pour chaque bloc de message transmis
    {
        nouveaux_chemins=chemins;
        chemins.clear();
        int nombre_chemin=0;
        vector<Chemin>::iterator it2;
        for(it2=nouveaux_chemins.begin();it2!=nouveaux_chemins.end();it2++) // Pour chaque chemin
        {
            nombre_chemin++;

            // Création de deux nouveaux chemins
            Chemin c1 = *it2;
            Chemin c2 = *it2;



            // Calcul des distances locales
            bitset<R+1>entree_et_registre1(c1.registre.to_string()+"0"); // entree_et_registre1 est un bitset de 5 bits contenant le registre + le bit d'entrée
            int dist1 = distance_hamming(codage(entree_et_registre1), (*it));

            bitset<R+1>entree_et_registre2(c2.registre.to_string()+"1");
            int dist2 = distance_hamming(codage(entree_et_registre2), (*it));

            // Ajout des distances aux distances cumulées
            c1.dist_cumul+=dist1;
            c2.dist_cumul+=dist2;

	    // On acutalise les registres pour chaque nouveau chemin
            c1.registre=(c1.registre<<1);
            c2.registre=(c2.registre<<1);
            c1.registre[0]=0;
            c2.registre[0]=1;

	    // On actualise le message décodé
            c1.mess_dec.push_back(0);
            c2.mess_dec.push_back(1);

	    // On ajoute les nouveaux chemins au graphe
            chemins.push_back(c1);
            chemins.push_back(c2);
        }
    }
    // On trie les chemins selon les distances de hamming cumulées
    std::sort(chemins.begin(),chemins.end(),comp);

    // On ne retient que le message décodé du chemin ayant la meilleure distance de Hamming
    mess_dec=chemins.at(0).mess_dec;

    return mess_dec;
}




//////////////////////////////////////////////////////////////////
//                             MAIN                             //
//////////////////////////////////////////////////////////////////

int main()
{

 int NbMot = 12;

 vector< bitset<K> > mess;
 vector< bitset<N> > mess_cod;
 vector< bitset<N> > mess_tra;
 vector< bitset<K> > mess_dec;

 // Random initialization message
 srand( (unsigned)time( NULL ) );
 for(int i=0;i<NbMot;++i)
  mess.push_back(randBitset<K>());
 for(int i=0;i<R;++i)
  mess.push_back(bitset<K>(0));

 // Coding of the message => mess_cod
 mess_cod = GSM_code(mess);

 // Simulation of a transmission (errors) => mess_tra
 mess_tra = GSM_transmission(mess_cod);

 // Decoding of the transmitted message => mess_dec
 mess_dec = GSM_decode(mess_tra);

 cout << "Source Message   : ";
 for (vector<bitset<K> >::iterator it = mess.begin() ; it != mess.end(); ++it)
    cout << ' ' << *it;
  cout << '\n';

 cout << "Coded Message    : ";
 for (vector<bitset<N> >::iterator it = mess_cod.begin() ; it != mess_cod.end(); ++it)
    cout << ' ' << *it;
  cout << '\n';

 cout << "Received Message : ";
 for (vector<bitset<N> >::iterator it = mess_tra.begin() ; it != mess_tra.end(); ++it)
    cout << ' ' << *it;
  cout << '\n';

 cout << "Decoded Message  : ";
 for (vector<bitset<K> >::iterator it = mess_dec.begin() ; it != mess_dec.end(); ++it)
    cout << ' ' << *it;
  cout << '\n';

}
