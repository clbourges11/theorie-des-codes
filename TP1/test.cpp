#include <iostream>
using namespace std;

class Symbol {
	public:
		Symbol(string n, double f, string c, bool l, Symbol *le, Symbol *ri);
		string name;
		double freq;
		string code;
		bool leaf;
		Symbol *left, *right;

		// Surcharge d'opérateur
		bool operator<(Symbol const& b) const
		{
			if (this->freq<b.freq)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
};

// Constructeur par défaut
Symbol::Symbol(string n, double f, string c, bool l, Symbol *le, Symbol *ri)
{
	name=n;
	freq=f;
	code=c;
	leaf=l;
	left=le;
	right=ri;
}

int main() 
{
	Symbol sym1("coucou",3,"",true,NULL,NULL);
	Symbol sym2("coucou",4,"",true,NULL,NULL);
    	cout << (sym2<sym1);
    return 0;
}

