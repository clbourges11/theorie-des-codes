#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdlib>

#include "CImg.h"

#define ROUND( a ) ( ( (a) < 0 ) ? (int) ( (a) - 0.5 ) : (int) ( (a) + 0.5 ) )

using namespace std;
using namespace cimg_library;
CImg<double> DCTCalcul (CImg <double> Imagette)
{
	CImg<double> DCTImagette(8,8,1,1,0);
	int i=0;
	int j=0;
	int x=0;
	int y=0;
	for(i=0;i<8;i++)
	{
		double Ci;
		if(i==0)
		{
			Ci=1/sqrt(2);
		}
		else
		{
			Ci=1;
		}
		for(j=0;j<8;j++)
		{
			double Cj;
			if(j==0)
			{
				Cj=1/sqrt(2);
			}
			else
			{
				Cj=1;
			}
			double somme=0.;
			for(x=0;x<8;x++)
			{
				for(y=0;y<8;y++)
				{
					somme+=(Imagette(x,y,0,0)-128)*cos((2*x+1)*i*3.14159265359/16.0)*cos((2*y+1)*j*3.14159265359/16.0);
				}
			}
			DCTImagette(i,j)=(2.0/8.0)*Ci*Cj*somme;
		}
	}
	return DCTImagette;
}



CImg <double> Inverse_DCT (CImg<double> DCT_Imagette)
{
	int x=0;
	int y=0;
	int i=0;
	int j=0;
	CImg <double> Imagette(8,8,1,1,0);
	for(x=0;x<8;x++)
	{
		for(y=0;y<8;y++)
		{
			double somme=0.0;
			for(i=0;i<8;i++)
			{
				double Ci;
				if(i==0)
				{
					Ci=1/sqrt(2);
				}
				else
				{
					Ci=1;
				}
				for(j=0;j<8;j++)
				{
					double Cj;
					if(j==0)
					{
						Cj=1.0/sqrt(2.0);
					}
					else
					{
						Cj=1.0;
					}
					somme+=Ci*Cj*DCT_Imagette(i,j,0,0)*cos((2*x+1)*i*3.14159265359/16.0)*cos((2*y+1)*j*3.14159265359/16.0);
				}
			}
			Imagette(x,y,0,0)=(2.0/8.0)*somme;
		}
	}
	return Imagette;
}






CImg<double> JPEGEncoder_DCT(CImg<double> image)
{
	 CImg<double> comp(image.width(),image.height(),1,1,0);
	 comp = image;
	int x0;
	int y0;
	CImg<double> new_image(image.width(),image.height(),1,1,0);
		for(x0=0;x0<image.width();x0+=8) //On parcourt l'image en largeur
		{
			for(y0=0;y0<image.height();y0+=8) //On parcourt l'image en hauteur
			{
				CImg <double> Imagette(8,8,1,1,0);
				Imagette=image.get_crop(x0, y0,0,0,x0+7,y0+7,0,0);		//On extrait l'imagette
				CImg<double> DCT(8,8,1,1,0);		
				DCT=DCTCalcul(Imagette);
				int x;
				int y;
				for(x=0;x<8;x++)
				{
					for(y=0;y<8;y++)
					{
						new_image(x0+x,y0+y)=DCT(x,y)+128;
					}
				}
			}
		}
	 return new_image;
}




CImg<double> JPEGEncoder_Complet(CImg<double> image, double quality)
{
	 CImg<double> comp(image.width(),image.height(),1,1,0);
	 comp = image;

 // Quantization matrix

 CImg<double> Q(8,8);
 Q(0,0)=16;   Q(0,1)=11;   Q(0,2)=10;   Q(0,3)=16;   Q(0,4)=24;   Q(0,5)=40;   Q(0,6)=51;   Q(0,7)=61;
 Q(1,0)=12;   Q(1,1)=12;   Q(1,2)=14;   Q(1,3)=19;   Q(1,4)=26;   Q(1,5)=58;   Q(1,6)=60;   Q(1,7)=55;
 Q(2,0)=14;   Q(2,1)=13;   Q(2,2)=16;   Q(2,3)=24;   Q(2,4)=40;   Q(2,5)=57;   Q(2,6)=69;   Q(2,7)=56;
 Q(3,0)=14;   Q(3,1)=17;   Q(3,2)=22;   Q(3,3)=29;   Q(3,4)=51;   Q(3,5)=87;   Q(3,6)=80;   Q(3,7)=62;
 Q(4,0)=18;   Q(4,1)=22;   Q(4,2)=37;   Q(4,3)=56;   Q(4,4)=68;   Q(4,5)=109;  Q(4,6)=103;  Q(4,7)=77;
 Q(5,0)=24;   Q(5,1)=35;   Q(5,2)=55;   Q(5,3)=64;   Q(5,4)=81;   Q(5,5)=104;  Q(5,6)=113;  Q(5,7)=92;
 Q(6,0)=49;   Q(6,1)=64;   Q(6,2)=78;   Q(6,3)=87;   Q(6,4)=103;  Q(6,5)=121;  Q(6,6)=120;  Q(6,7)=101;
 Q(7,0)=72;   Q(7,1)=92;   Q(7,2)=95;   Q(7,3)=98;   Q(7,4)=112;  Q(7,5)=100;  Q(7,6)=103;  Q(7,7)=99;
 Q *= quality; 


	int x0;
	int y0;
	CImg<double> new_image(image.width(),image.height(),1,1,0);
		for(x0=0;x0<image.width();x0+=8) //On parcourt l'image en largeur
		{
			for(y0=0;y0<image.height();y0+=8) //On parcourt l'image en hauteur
			{
				CImg <double> Imagette(8,8,1,1,0);
				Imagette=image.get_crop(x0, y0,0,0,x0+7,y0+7,0,0);		//On extrait l'imagette
				CImg<double> DCT(8,8,1,1,0);		
				DCT=DCTCalcul(Imagette);
				// Quantification
				CImg<double>QUANT_DCT(8,8,1,1,0);		
				int n;
				int m;
				for(n=0;n<8;n++)
				{
					for(m=0;m<8;m++)
					{
						QUANT_DCT(n,m)=ROUND(DCT(n,m)/Q(n,m));
					}
				}
				CImg<double>QUANT_INV(8,8,1,1,0);		
				for(n=0;n<8;n++)
				{
					for(m=0;m<8;m++)
					{
						QUANT_INV(n,m)=ROUND(QUANT_DCT(n,m)*Q(n,m));
					}
				}
				CImg <double> new_imagette(8,8,1,1,0);
				new_imagette=Inverse_DCT(QUANT_INV);
				int x;
				int y;
				for(x=0;x<8;x++)
				{
					for(y=0;y<8;y++)
					{
						new_image(x0+x,y0+y)=new_imagette(x,y)+128;
					}
				}
			}
		}
	 return new_image;
}

double distorsion(CImg<double> image1, CImg<double> image2)
{
	double somme =0.0;
	int x=0;
	int y=0;
	for(x=0;x<image1.width();x++) //On parcourt l'image en largeur
	{
		for(y=0;y<image1.height();y++) //On parcourt l'image en hauteur
		{
			somme+=(image1(x,y)-image2(x,y))*(image1(x,y)-image2(x,y));
		}
	}
	return somme/(image1.height()*image1.width());
}

int main()
{
	std::cout<<"1) Appliquer la DCT et afficher le resultat"<<endl<<"2) Effectuer la DCT, la Quantification, la Quantification inverse, la DCT inverse et afficher l'image décompressée"<<endl<<"3) Evolution du taux de distorsion"<<endl<<"Choix ?";
	int choix=0;
	std::cin >> choix;
	if(choix==1)
	{
		CImg<double> my_image("lena.bmp");
		CImg<double> comp_image = JPEGEncoder_DCT(my_image);
		CImgDisplay main_disp(my_image,"Initial Image");

		 // Display the compressed file (by dct)
		 CImgDisplay comp_disp(comp_image,"Compressed Image");

		std::cout<<distorsion(my_image,comp_image);
		 while (!main_disp.is_closed());
		 {
		  main_disp.wait();
		 }
	}
	else if(choix==2)
	{
		double quality;
		std::cout<<"Qualité?"<<endl;
		std::cin >> quality;
		 // Read the image "lena.bmp"
		 CImg<double> my_image("lena.bmp");

		 // Take the luminance information 
		 my_image.channel(0);
		 
		CImg<double> comp_image = JPEGEncoder_Complet(my_image,quality);


		 // Display the bmp file
		 CImgDisplay main_disp(my_image,"Initial Image");

		 // Display the compressed file (by dct)
		 CImgDisplay comp_disp(comp_image,"Compressed Image");

		std::cout<<distorsion(my_image,comp_image);
		 while (!main_disp.is_closed());
		 {
		  main_disp.wait();
		 }
	}
	else if ( choix == 3 )
	{
		double qualmin = 0.0;
		double qualmax = 0.0;
		double pas = 0.0;
		std::cout<<"Facteur de qualité minimum ?"<<endl;
		std::cin >> qualmin;
		std::cout<<"Facteur de qualité maximum ?"<<endl;
		std::cin >> qualmax;
		std::cout<<"Pas ?"<<endl;
		std::cin >> pas;
		for(double i = qualmin; i<=qualmax;i+=pas)
		{
			CImg<double> my_image("lena.bmp");
			CImg<double> comp_image = JPEGEncoder_Complet(my_image,i);
			std::cout<<"Qualité:"<<i<<" --> Distorsion:  "<<distorsion(my_image,comp_image)<<endl;
		}
	}
}















